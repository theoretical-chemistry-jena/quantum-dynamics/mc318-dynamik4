FC  		= gfortran
FFLAGS		= -O3 -w -fopenmp -march=native ${FFTWFLGS}
LDFLAGS 	= -lm
FFTWFLGS	=  -I${FFTW_IDIR} -L${FFTW_LDIR} -lfftw3

FFTW_DIR    ?= /usr
FFTW_IDIR   ?= $(FFTW_DIR)/include
FFTW_LDIR   ?= $(FFTW_DIR)/lib64

default: 	dynamik4

dynamik4:   	main.o   propagation.o

		${FC} *.o ${FFLAGS} ${LDFLAGS} -o dynamik4

main.o:	 src/main.f90
	${FC} $< ${FFLAGS} -c -o $@

propagation.o:	src/propagation.f90
	${FC} $< ${FFLAGS} -c -o $@

clean:
	rm -f *.o
	rm -f *.mod
	rm -f dynamik4
	rm -f movieout
	rm -f time

