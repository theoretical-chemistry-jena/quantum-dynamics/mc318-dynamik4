subroutine propagation

    use data_grid
    use pot_param
    use data_au
    use omp_lib
    use, intrinsic:: iso_c_binding

    implicit none

    include 'fftw3.f03'

    integer I, J, K

    type(C_PTR):: planF, planB
    double precision:: time               ! nun - die Zeit
    double precision:: tte, tts                ! Zeit zur Performancemessung
    double precision:: R                        ! Kernabstand
    double precision:: E                        ! Energie (E_kin der Photoelektronen)
    double precision:: cpm                        ! Abschneideparameter
    double precision:: norm(2)                ! die Gesamtnorm der Wellenfunktion
    double precision:: norm_ionic                ! die Norm im ionischen Zustand
    double precision:: evR(2)                        ! Ortserwartungswert
    double precision:: cof(nr)                ! Abschneidefunktion
    double precision:: psi0(nr)                 ! die Anfangswellenfunktion

    complex*16:: psi_save(nr, 2)
    complex*16, allocatable, dimension(:, :):: psi          ! Wellenfunktion, angeregter Zustand
    complex*16, allocatable, dimension(:):: kprop           ! Kinetischer Propagator
    complex*16, allocatable, dimension(:):: vprop           ! Potentieller Propagator
    complex*16, allocatable, dimension(:, :):: psi_ionic    ! Ionische Wellenfunktion
    complex*16, allocatable, dimension(:):: psi_e           ! Zwischenfunktion psi_ionic
    complex*16, allocatable, dimension(:):: laser           ! Das Feld des ionisierenden Lasers

    double precision, allocatable, dimension(:):: pes       ! Photoelektronenspektrum
    double precision, allocatable, dimension(:):: v_diff, v_bar, wk

    open (100, file='out/psi0.dat', status='unknown')               ! Anfangswellenfunktion - 2D
    open (101, file='out/cut_off_function.dat', status='unknown')   ! Die Abschneidefunktion - 2D
    open (200, file='out/dens.dat', status='unknown')               ! Kernwellenpaket im unteren Zustand - 3D
    open (301, file='out/laser.dat', status='unknown')              ! Laserfeld des ionisierenden Lasers - 2D
    open (800, file='out/R.dat', status='unknown')                  ! Ortserwartungswert - 2D, zwei Spalten
    open (908, file='out/norm.dat', status='unknown')               ! Norm im el. Zustand- 2D, zwei Spalten
    open (909, file='out/norm_ionic.dat', status='unknown')         ! Norm im ionischen Zustaenden- 2D, eine Spalte
    open (900, file='out/spectrum.dat', status='unknown')           ! Besetzung der Vibrationszustaende - 2D

    allocate (psi(NR, 2), kprop(NR), vprop(NR))
    allocate (psi_ionic(nr, energy_points), psi_e(nr), laser(energy_points))
    allocate (pes(energy_points), v_diff(nr), v_bar(nr), wk(nr))

    call dfftw_plan_dft_1d(planF, nr, psi_e, psi_e, FFTW_FORWARD, FFTW_MEASURE)
    call dfftw_plan_dft_1d(planB, nr, psi_e, psi_e, FFTW_BACKWARD, FFTW_MEASURE)

    psi = (0.d0, 0.d0)                    ! Initialisierung

    cpm = 1.5d0/au2a

    do j = 1, NR                            ! Das ist eine Abschneidefunktion, die verhindern soll,
        R = R0 + (j - 1)*dR                 ! dass ein Wellenpaket, wenn es dissoziiert, an das Ende
        if (R .lt. (Rend - cpm)) then       ! des Grids stoesst.
            cof(j) = 1.d0                   ! Daher schneidet diese Funktion (cut-off-function) die
        else                                ! auslaufende Wellenfunktion sanft ein paar Angstrom
            cof(j) = cos(((R - Rend + cpm)/-cpm)*(0.5d0*pi))        ! (definiert durch den
            cof(j) = cof(j)**2                                      ! Parameter CPM) vorher ab.
        end if
        write (101, *) sngl(R*au2a), sngl(cof(j))
    end do

    do i = 1, NR
        R = R0 + (I - 1)*dR
        psi(i, 1) = exp(-1.d1*(R - R_eq)**2)                ! Initiale Wellenfunktion (Gauss WP)
        v_diff(i) = pot(i, 2) - pot(i, 1)                   ! Potentialdifferenz
        v_bar(i) = 0.5d0*(pot(i, 1) + pot(i, 2))            !
        wk(i) = sqrt((2.d0*v_koppl(i))**2 + (v_diff(i))**2) !
        kprop(i) = exp(-im*dt*Pr(i)**2/(2.d0*mass))         ! Kinetischer Propagator
        vprop(i) = exp(-0.5d0*im*dt*pot(i, 3))              ! Potentieller Propagator - ionischer
    end do                                                  ! Zustand (pot(R,3))

    ! Berechnet die Norm im angeregten Zustand
    call integ(psi, norm)
    psi(1:nr, 1) = psi(1:nr, 1)/sqrt(norm(1))

    do I = 1, NR
        R = R0 + (I - 1)*dR
        psi0 = psi(i, 1)
        write (100, *) sngl(R*au2a), sngl(abs(psi0(I))**2)
    end do

!______________________________________________________________________
!
!                   Propagation Loop
!_____________________________________________________________________

    print *
    print *, '1D propagation...'
    print *

    tts = omp_get_wtime()

    timeloop: do K = 1, Nt                        ! Zeitschleife

        time = K*dt

        if (mod(K, 100) .eq. 0) then                ! Schreibt alle 100 Werte die Zeit
            print *, 'time:', sngl(time*au2fs)             ! auf den Bildschirm
        end if

!============== Definition des Laserfeldes ====================================

        do j = 1, energy_points   ! Energieerhaltung - die Energien der Photoelektronen
            E = E0 + (j - 1)*dE         ! entsprechen E = omega_2 - Delta V(R)
            laser(j) = exp(-fwhm2*(time - t_2)**2 - im*(omega2 - E)*time)
        end do

! ============= Propagationsteil ==============================================
!
! Die Propagation findet dieses Mal fuer die Wellenfunktionen in beiden
! elektronischen Zustaenden, im angeregten und im ionischen Zustand statt.
! _____________________________________________________________________________

        do J = 1, energy_points         ! Ionisation auch via Stoerungstheorie
            do I = 1, nr
                psi_ionic(i, j) = psi_ionic(i, j) + dt*psi(i, 1)*laser(j)
                psi_ionic(i, j) = psi_ionic(i, j) - im*dt*psi(i, 2)*laser(j)
            end do
        end do

! ......Kinetische Propagation ....................

        do i = 1, 2                   ! Grund und angeregter Zustand
            do j = 1, nr                                ! Jede Komponente (jede Energie) muss separat
                psi_e(j) = psi(j, i)                ! in den Impulsraum Fouriertransfomiert werden.
            end do                                        ! psi_e ist eine Hilfsgroesse, in die zwischenkopiert wird.
            call fftw_execute_dft(planF, psi_e, psi_e) ! Fouriertransformation: Psi(R) --> Psi(P)
            psi_e = psi_e*exp(-im*dt*Pr**2/(4.d0*mass))                           ! Kinetische Propagation
            call fftw_execute_dft(planB, psi_e, psi_e) ! Fouriertransformation: Psi(R) --> Psi(P)
            psi_e = psi_e/dble(nr)

            do J = 1, Nr                                   ! Zurueckkopieren der Hilfsgroesse psi_e
                psi(j, i) = psi_e(j)                 ! in die urspruengliche Groesse psi
            end do
        end do

        ! ......Potentielle Propagation ........................

        ! call diabatic_pot_coupling(psi, v_diff, wk, v_bar)        ! Diabatische Kopplung
        psi_save = psi
        do i = 1, nr
            psi(i, 1) = exp(-im*dt*v_bar(i))&
            & * (cos(0.5d0*wk(i)*dt) * psi_save(i, 1) &
            & + im * sin(0.5d0*wk(i)*dt) &
            & * (+v_diff(i) * psi_save(i, 1) - 2.d0*v_koppl(i) * psi_save(i, 2))/wk(i))

            psi(i, 2) = exp(-im*dt*v_bar(i))&
            & * (cos(0.5d0*wk(i)*dt)*psi_save(i, 2) &
            & + im*sin(0.5d0*wk(i)*dt) &
            & * (-v_diff(i) * psi_save(i, 2) - 2.d0*v_koppl(i) * psi_save(i, 1))/wk(i))

        end do

        ! ......Kinetische Propagation ....................

        do i = 1, 2                     ! Grund und angeregter Zustand
            do j = 1, nr                ! Jede Komponente (jede Energie) muss separat
                psi_e(j) = psi(j, i)    ! in den Impulsraum Fouriertransfomiert werden.
            end do                      ! psi_e ist eine Hilfsgroesse, in die zwischenkopiert wird.

            call fftw_execute_dft(planF, psi_e, psi_e)  ! Fouriertransformation: Psi(R) --> Psi(P)
            psi_e = psi_e*exp(-im*dt*Pr**2/(4.d0*mass)) ! Kinetische Propagation
            call fftw_execute_dft(planB, psi_e, psi_e)  ! Fouriertransformation: Psi(R) --> Psi(P)
            psi_e = psi_e/dble(nr)

            do J = 1, Nr                ! Zurueckkopieren der Hilfsgroesse psi_e
                psi(j, i) = psi_e(j)    ! in die urspruengliche Groesse psi
            end do
        end do

!------------------------------------------------------------------
!------------------- ionische Wellenfunktion --------------------
!----------------------------------------------------------------

        ! ......Potentielle Propagation ........................

        do i = 1, energy_points                        ! Ionischer Zustand
            do j = 1, nr
                psi_ionic(j, i) = psi_ionic(j, i)*vprop(j)
            end do
        end do

        ! ......Kinetische Propagation ....................

        do i = 1, energy_points   ! Ionischer Zustand
            do j = 1, nr                        ! Jede Komponente (jede Energie) muss separat
                psi_e(j) = psi_ionic(j, i)      ! in den Impulsraum Fouriertransfomiert werden.
            end do                              ! psi_e ist eine Hilfsgroesse, in die zwischenkopiert wird.

            call fftw_execute_dft(planF, psi_e, psi_e) ! Fouriertransformation: Psi(R) --> Psi(P)
            psi_e = psi_e*kprop                          ! Kinetische Propagation
            call fftw_execute_dft(planB, psi_e, psi_e) ! Fouriertransformation: Psi(R) --> Psi(P)
            psi_e = psi_e/dble(nr)

            do J = 1, Nr                        ! Zurueckkopieren der Hilfsgroesse psi_e
                psi_ionic(j, i) = psi_e(j)      ! in die urspruengliche Groesse psi_ionic
            end do
        end do

        ! ......Potentielle Propagation ........................

        do i = 1, energy_points                        ! Ionischer Zustand
            do j = 1, nr
                psi_ionic(j, i) = psi_ionic(j, i)*vprop(j)
            end do
        end do

! ============ Ende der Propagation, hier kommt nur noch Output ==============

        call integ(psi, norm)                        ! Berechnet die Norm im angeregten Zustand
        call integ_ionic(psi_ionic, norm_ionic)        ! Berechnet die Norm im ionischen Zustand

        evR = 0.d0          ! Ortserwarungswert: <R> = int R* |psi(R)|^2 *dR / int |psi(R)|^2 *dR

        do j = 1, 2         ! Ortserwarungswert <R> = int |psi(R)|^2,  Numerische Integration
            do I = 1, NR
                R = R0 + (I - 1)*dR
                evR(j) = evR(j) + abs(psi(I, j))**2*R
            end do
            evR(j) = evR(j)*dR              ! Ortserwartungswert  *dR
            evR(j) = evR(j)/norm(j)         !  / int |psi(R)|^2 *dR
        end do

        write (800, *) sngl(time*au2fs), sngl(evR*au2a)     ! Schreibt den Ortserwartungswert raus
        write (908, *) sngl(time*au2fs), sngl(norm)         ! Schreibt die Pop. im ES
        write (909, *) sngl(time*au2fs), sngl(norm_ionic)   ! Schreibt die Pop. im ion. Zustand
        write (301, *) sngl(time*au2fs), sngl(real(laser(75)))  ! Schreibt das Laserfeld raus

        do I = 1, NR
            R = R0 + (I - 1)*dR
            if (mod(I, 4) .eq. 0) then              ! Nur jeder 4. Ortspunkt, sonst Datei zu groß
                write (200, *) sngl(time*au2fs), sngl(R*au2a), &
                 & sngl(abs(psi(I, 1)**2)), sngl(abs(psi(I, 2)**2)) ! Dichte |psi(R,t)|^2
            end if
        end do
        write (200, *)

        do i = 1, energy_points                         ! ebenso fuer den ionischen Zustand
            psi_ionic(:, i) = psi_ionic(:, i)*cof(:)
        end do

    end do timeloop                                     ! Ende der Zeitschleife

    tte = omp_get_wtime()

    write (*, *) "Time to finish: ", tte - tts, " s"

    call spectrum(psi_ionic, pes)

    do i = 1, energy_points
        E = E0 + (i - 1)*dE
        write (900, *), sngl(E*au2eV), sngl(pes(i))
    end do

    close (100, status='keep')
    close (101, status='keep')
    close (102, status='keep')
    close (200, status='keep')
    close (201, status='keep')
    close (300, status='keep')
    close (301, status='keep')
    close (800, status='keep')
    close (908, status='keep')
    close (909, status='keep')

    deallocate (psi, kprop, vprop)

    return
end subroutine

!_________________________________________________________

subroutine integ(psi, norm)

    use data_grid

    implicit none
    integer:: I, j
    double precision, intent(out):: norm(2)
    complex*16, intent(in):: psi(NR, 2)

    ! Dieses Unterprogramm rechnet die Norm der komplexen Wellenfunktion aus.

    norm = 0.d0

    do j = 1, 2
        do I = 1, NR
            norm(j) = norm(j) + abs(psi(I, j))**2
        end do
        norm(j) = norm(j)*dR
    end do

    return
end subroutine
!_________________________________________________________

subroutine integ_ionic(psi_ionic, norm_ionic)

    use data_grid

    implicit none
    integer:: i, j
    double precision, intent(out):: norm_ionic
    complex*16, intent(in):: psi_ionic(NR, energy_points)

    ! Dieses Unterprogramm rechnet die Norm der ionischen Wellenfunktion aus.

    norm_ionic = 0.d0

    do j = 1, energy_points
        do I = 1, NR
            norm_ionic = norm_ionic + abs(psi_ionic(i, j))**2
        end do
    end do

    norm_ionic = norm_ionic*dR*dE

    return
end subroutine
!______________________________________________________________

subroutine spectrum(psi_ionic, pes)

    use data_grid

    implicit none
    integer:: i, j
    double precision, intent(out):: pes(energy_points)
    complex*16, intent(in):: psi_ionic(NR, energy_points)

    ! Dieses Unterprogramm rechnet das Photoelektronenspektrum aus.

    do j = 1, energy_points
        do i = 1, nr
            pes(j) = pes(j) + abs(psi_ionic(i, j))**2
        end do
        pes(j) = pes(j)*dR
    end do

    return
end subroutine

!________________________________________________________________

subroutine diabatic_pot_coupling(psi, v_diff, wk, v_bar)        ! Diabatische Kopplung

    use data_grid
    use data_au

    implicit none
    integer:: i
    double precision, intent(in):: v_diff(nr), wk(nr), v_bar(nr)
    complex*16, intent(inout):: psi(nr, 2)

    complex*16:: psi_save(nr, 2)

    psi_save = psi

    do i = 1, nr

        psi(i, 1) = exp(-im*dt*v_bar(i))*(cos(0.5d0*wk(i)*dt)*psi_save(i, 1) + &
         & im*sin(0.5d0*wk(i)*dt)/wk(i)*(v_diff(i)*psi_save(i, 1) - 2.d0*v_koppl(i)*psi_save(i, 2)))

        psi(i, 2) = exp(-im*dt*v_bar(i))*(cos(0.5d0*wk(i)*dt)*psi_save(i, 2) + &
        & im*sin(0.5d0*wk(i)*dt)/wk(i)*(-v_diff(i)*psi_save(i, 2) - 2.d0*v_koppl(i)*psi_save(i, 1)))

    end do

    return
end subroutine

!_________________________________________________________
! __________________________ Fourier-Transformationsroutine________

! COMPLEX FAST FOURIER TRANSFORMATION, NN must be a power of 2
! ISIGN = -1 : x -> p
! ISIGN = +1 : p -> x

SUBROUTINE DFFT1(DATAS, NN, ISIGN)
    implicit REAL*8(a - h, o - z)
    DIMENSION DATAS(*)

    N = 2*NN
    J = 1

    DO 11 I = 1, N, 2
        IF (J .GT. I) THEN
            TEMPR = DATAS(J)
            TEMPI = DATAS(J + 1)
            DATAS(J) = DATAS(I)
            DATAS(J + 1) = DATAS(I + 1)
            DATAS(I) = TEMPR
            DATAS(I + 1) = TEMPI
        ENDIF
        M = N/2
1       IF ((M .GE. 2) .AND. (J .GT. M)) THEN
            J = J - M
            M = M/2
            GO TO 1
        ENDIF
        J = J + M
11      CONTINUE
        MMAX = 2
2       IF (N .GT. MMAX) THEN
            ISTEP = 2*MMAX
            THETA = 6.28318530717959D0/(ISIGN*MMAX)
            WPR = -2.D0*DSIN(0.5D0*THETA)**2
            WPI = DSIN(THETA)
            WR = 1.D0
            WI = 0.D0
            DO 13 M = 1, MMAX, 2
                DO 12 I = M, N, ISTEP
                    J = I + MMAX
                    TEMPR = WR*DATAS(J) - WI*DATAS(J + 1)
                    TEMPI = WR*DATAS(J + 1) + WI*DATAS(J)
                    DATAS(J) = DATAS(I) - TEMPR
                    DATAS(J + 1) = DATAS(I + 1) - TEMPI
                    DATAS(I) = DATAS(I) + TEMPR
                    DATAS(I + 1) = DATAS(I + 1) + TEMPI
12                  CONTINUE
                    WTEMP = WR
                    WR = WR*WPR - WI*WPI + WR
                    WI = WI*WPR + WTEMP*WPI + WI
13                  CONTINUE
                    MMAX = ISTEP
                    GO TO 2
                    ENDIF
                    RETURN
                END

!********************************************************************
