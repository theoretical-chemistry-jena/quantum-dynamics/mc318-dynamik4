module data_grid
    integer:: Nt                       ! Anzahl der Zeitschritte
    integer, parameter:: Nr = 512        ! Anzahl der Punkte des Ortsgrids
    integer, parameter:: energy_points = 150   ! Anzahl der Punkte des Energiegrids
    double precision:: dR, dpR, dt           ! Schrittgroesse Orts-, Impuls- und Zeitgrid
    double precision:: dE     ! Schrittgroesse Energiegrid (f. Photoelektronen)
    double precision:: PR(nr)             ! Impulsgrid
    double precision:: Pot(nr, 3)                 ! Die Potentialkurven
    double precision:: v_koppl(nr)            ! Diabatische Kopplung
    double precision:: mass           ! Die Masse des Systems
    double precision:: fwhm2           ! Laenge der beiden Pulse
    double precision:: omega2            ! Frequenzen der beiden Pulse
    double precision:: t_2               ! Startzeiten der beiden Pulse
    double precision:: R_eq
end module

module data_au
    double precision, parameter:: au2a = 0.52917706d0  ! Umrechnungsfaktor Laenge in a.u. --> Angstrom
    double precision, parameter:: cm2au = 4.5554927d-6 ! Umrechnungsfaktor Energie von Wellenzahlen --> a.u.
    double precision, parameter:: au2fs = 0.024        ! Umrechnungsfaktor Zeit von a.u. --> Femtosekunden
    double precision, parameter:: j2eV = 6.242D18    ! Umrechnungsfaktor Energie von J --> eV
    double precision, parameter:: au2eV = 27.2116d0    ! Umrechnungsfaktor Energie von a.u. --> eV
    double precision, parameter:: i2au = 2.0997496D-9
    double precision, parameter:: pi = 3.141592653589793d0    ! einfach nur pi
    double precision, parameter:: d2au = 0.3934302014076827d0 ! Umrechnungsfaktor Dipolmoment von Debye --> a.u.
    double precision, parameter:: amu = 1822.888d0    ! Atomic mass unit (atomare Masseneinheit)
    complex*16, parameter:: im = (0.d0, 1.d0)    ! Das ist i, die imaginaere Zahl
end module

module pot_param
    use data_au
    double precision, parameter:: R0 = 0.01d0           ! Ortsgrid Parameter, Anfang..
    double precision, parameter:: Rend = 20.d0          ! ..und Ende
    double precision, parameter:: E0 = 0.00d0           ! Energiegrid Parameter, Anfang..
    double precision, parameter:: Eend = 5.d0/au2eV   !..und Ende
end module pot_param

program Non_BO_Dynamik

    use data_grid
    implicit none

    print *
    print *
    print *, 'Initialization...'
    print *

    call system("mkdir -p out/")
    call system("cp in/input out/")  ! backup input file

    call input
    call p_grid

    call potential
    call propagation

    print *, 'Finished'

end program

! _______________ Subroutines __________________________________________________

subroutine input

    use data_grid
    use pot_param
    implicit none

    double precision:: lambda
    double precision:: tp2
    double precision:: R_init

    open (10, file='in/input', status='old')

! In diesem Unterprogramm werden Parameter eingelesen und do i = 1, nr
! umgerechnet.
! Parameter: Laserparameter

    read (10, *) Nt                      ! Nt = number of time steps.
    read (10, *) lambda          ! = Wellenlaenge des Abfragelasers in nm
    read (10, *) tp2          ! = Laenge Laserpuls 1, Laenge Laser 2
    read (10, *) t_2          ! = Start Laserpuls 1, Start Laser 2
    read (10, *) mass          ! = Masse des Systems
    read (10, *) R_init

    dR = (Rend - R0)/(NR - 1)  ! Schrittweite des Ortsgrids
    dE = (Eend - E0)/(energy_points - 1)        ! Schrittweite des Energiegrids

    mass = 0.5*mass*amu          ! Reduzierte Masse des Systems

    dt = 0.1d0/au2fs          ! Zeitschritt
    R_eq = R_init/au2a            ! Gleichgewichtsabstand im Na2 Grundzustand

    tp2 = tp2/au2fs           ! Laenge des Laserpulses (in fs)
    t_2 = t_2/au2fs          ! Laserpuls zentriert um diesen Zeitpunkt (in fs)
    fwhm2 = (4.d0*dlog(2.d0))/tp2**2         !     analog Puls 2
    omega2 = (1.d0/(lambda*1.d-7))*cm2au

    print *, '_________________________'
    print *
    print *, 'Parameters'
    print *, '_________________________'
    print *
    print *, 'dt = ', sngl(dt*au2fs), 'fs'
    print *, 'dR = ', sngl(dR*au2a), 'A'
    print *, 'dE = ', sngl(dE*au2eV), 'eV'
    print *
    print *, 'Masse:', sngl(mass/amu)
    print *, 'Anfangsposition:', sngl(R_init), 'A'
    print *
    Print *, 'LASER Parameter _________'
    print *
    print *, 'Abfragelaser:'
    print *, 'Wellenlaenge = ', sngl(lambda), 'nm'
    print *, '             = ', sngl(omega2*au2eV), 'eV'
    print *, 'Pulslaenge = ', sngl(tp2*au2fs), 'fs'
    print *, 'Pulsstart = ', sngl(t_2*au2fs), 'fs'
    print *
    print *, '__________________________'
    print *

    close (10)
end subroutine

!...................... Impulsgrid......................

subroutine p_grid

    use data_grid
    use data_au
    implicit none
    integer:: I

! In diesem Unterprogramm wird das Grid im Impulsraum definiert. Auf diesem Grid
! ist die Wellenfunktion im Impulsraum definiert - die Schrittgroesse dPR haengt
! von der Schrittgroesse im Ortsraum, dR, und der Anzahl der Gridpunkte NR zusammen.

    dPR = (2.d0*pi)/(dR*NR)

    do I = 1, NR
        if (I .le. (NR/2)) then
            PR(I) = (I - 1)*dPR
        else
            PR(I) = -(NR + 1 - I)*dpR
        end if
    end do

    return
end subroutine

!........................... Einlesen des Potentials..............

subroutine potential

    use data_grid
    use data_au
    use pot_param

    implicit none

    integer:: i
    double precision:: R

    open (20, file='out/Potential.dat', status='unknown')

! Hier werden die Potentiale, auf dem wir die Kerndynamik berechnen wollen,
! initialisiert und alles in atomare Einheiten umgerechnet.

    do i = 1, nr
        r = r0 + (i - 1)*dr
        pot(i, 1) = (0.1d0*(r - 5.d0)**2 + 0.5)
        pot(i, 2) = (0.15d0*(r - 9.d0)**2)
        v_koppl(i) = 5.d1*exp(-2.d0*(r - 6.7)**2)
        pot(i, 3) = 5.d0/r + 4.d0
    end do

    pot = pot/au2eV
    v_koppl = v_koppl/mass

    do I = 1, NR
        R = R0 + (I - 1)*dR
        write (20, *) sngl(R*au2a), sngl(pot(i, :)*au2eV), sngl(v_koppl(i))
    end do

    close (20, status='keep')

    return
end subroutine
