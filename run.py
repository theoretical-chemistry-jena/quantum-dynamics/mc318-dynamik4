#!/usr/bin/env python3

import argparse
import os
from pathlib import Path
import shutil
import subprocess
import sys
import textwrap


def parse_args(args):
    parser = argparse.ArgumentParser()

    parser.add_argument("--nt", type=int, default=2000, help="Number of timesteps.")
    parser.add_argument(
        "--wavelength", type=float, default=240.0, help="Wavelength of probe laser."
    )
    parser.add_argument(
        "--duration", type=float, default=20.0, help="Duration of probe pulse in fs."
    )
    parser.add_argument(
        "--center", "-c", type=float, default=70.0, help="Center of probe pulse in fs."
    )
    parser.add_argument("--mass", type=float, default=10.0, help="System mass in amu.")
    parser.add_argument(
        "--init", type=float, default=0.5, help="Initial position of wavepackage."
    )
    parser.add_argument(
        "-f", "--force", action="store_true", help="Overwrite existing directories."
    )

    return parser.parse_args(args)


def run():
    args = parse_args(sys.argv[1:])

    nt = args.nt
    wavelength = args.wavelength
    duration = args.duration
    center = args.center
    mass = args.mass
    init = args.init
    force = args.force

    inp_text = textwrap.dedent(
        f"""
    {nt}  ! Number of timesteps
    {wavelength:.2f}  ! Wavelength of probe laser in nm
    {duration:.2f}  ! Duration of probe pulse in fs
    {center:.2f}  ! Center of probe laser in fs
    {mass:.4f}  ! Mass in amu
    {init:.4f}  ! Initial position of wavepackage
    """
    )
    print(f"Input:\n{inp_text}")

    out = Path(
        f"out_{nt}-l_{wavelength:.2f}-d_{duration:.2f}-c_{center:.2f}-m_{mass:.4f}-i_{init:.4f}"
    )
    if out.exists():
        print(f"Directory '{out}' already exists!")
        if force:
            shutil.rmtree(out)
        else:
            print(f"Please delete it manually.")
            return

    inp = Path("in/input")
    try:
        shutil.copy(inp, "in/input.previous")
    except FileNotFoundError:
        print(f"Did not find '{inp}'! Skipping backup.")

    with open(inp, "w") as handle:
        handle.write(inp_text)
    print(f"Wrote input file to {inp}.")


    subprocess.run("./dynamik4")

    print(f"Renamed 'out/' to '{out}/'")
    os.rename("out", out)


if __name__ == "__main__":
    run()
