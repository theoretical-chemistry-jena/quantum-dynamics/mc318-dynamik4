#!/usr/bin/env python3

import argparse
from pathlib import Path
import sys

import matplotlib as mpl
import matplotlib.animation as animation
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
import numpy as np


mpl.rcParams["font.size"] = 14
mpl.rcParams["legend.fontsize"] = 10
mpl.rcParams["lines.linewidth"] = 2


def parse_args(args):
    parser = argparse.ArgumentParser()

    parser.add_argument("--out", default="out")
    parser.add_argument("--interval", type=int, default=5)
    parser.add_argument("--nth", type=int, default=5)

    return parser.parse_args(args)


def run():
    args = parse_args(sys.argv[1:])

    out = Path(args.out)
    assert out.exists()
    nth = args.nth

    cgs = "tab:blue"
    ces = "tab:orange"
    claser = "tab:red"
    cion = "tab:purple"

    with open(out / "input") as handle:
        text = handle.read()
    inp_lines = text.strip().split("\n")
    nsteps, wavelength, llength, lcenter, mass, pos0 = [
        kind(line.strip().split()[0])
        for line, kind in zip(inp_lines, (int, float, float, float, float, float))
    ]

    xlim = (0.0, 10.5)  # w/o cutoff 8.0 may be better suited
    ylim = (-1.0, 7.0)  

    # R, V0 V1 Vion, coupling
    pot = np.loadtxt(out / "Potential.dat")
    rs, pot0, pot1, pot_ion, couplings = pot.T
    dyn = np.loadtxt(out / "dens.dat")  # t, <R>, Psi²
    R = np.loadtxt(out / "R.dat")
    norms = np.loadtxt(out / "norm.dat")
    norms_ionic = np.loadtxt(out / "norm_ionic.dat")
    
    laser = np.loadtxt(out / "laser.dat")
    cutoff = np.loadtxt(out / "cut_off_function.dat")

    pot_min = pot0.min()
    pot0 -= pot_min
    pot1 -= pot_min
    pot_ion -= pot_min

    every = 1
    n = nsteps // every
    R = R[::every]
    norms = norms[::every]
    dyn = dyn.reshape(n, -1, 4)
    assert R.shape[0] == dyn.shape[0]
    dt = 0.1

    t0, r0, psi0, psi1 = dyn[0].T
    tfin, *_ = dyn[-1].T[0]

    fig = plt.figure(constrained_layout=True, figsize=(14, 10))
    grid = GridSpec(3, 1, figure=fig, height_ratios=[2, 1, 0.25])
    ax0 = fig.add_subplot(grid[0])
    ax1 = fig.add_subplot(grid[1])

    # Pontential energy curves
    ax0.plot(rs, pot0, c=cgs, ls="--", label="GS")
    ax0.plot(rs, pot1, c=ces, ls="--", label="ES")
    ax0.plot(rs, pot_ion, c=cion, label="ion")
    ax0.set_xlim(*xlim)
    ax0.set_ylim(*ylim)
    ax0.set_xlabel("R / Angstrom")
    ax0.set_ylabel("Pot / eV")
    ax0.legend()

    # Densities
    ax0_twin = ax0.twinx()
    (lines0,) = ax0_twin.plot(r0, psi0, c=cgs, label="|Ψ²|_gs")
    vline_gs = ax0_twin.axvline(R[0, 1], c=cgs, ls="dotted", label="<R_gs>")
    (lines1,) = ax0_twin.plot(r0, psi1, c=ces, label="|Ψ²|_es")
    vline_es = ax0_twin.axvline(R[0, 2], c=ces, ls="dotted", label="<R_es>")
    ax0_twin.set_ylim(0.0, 3.0)
    ax0_twin.set_ylabel("$\Psi^2$")
    ax0_twin.plot(*cutoff.T, c="red", alpha=0.5, ls="dashdot", label="cutoff")
    ax0_twin.legend(loc="lower right")

    ax1.plot(*laser.T, c=claser, alpha=0.35)
    scatter_laser = ax1.scatter(*laser[0], s=30, c=claser)
    ax1.set_xlim(0, tfin)
    ax1.set_xlabel("t / fs")
    ax1.set_ylabel("E-field")
    ax1_twin = ax1.twinx()
    ax1_twin.plot(*norms_ionic.T, c=cion, label="|Ψ²|_ionic")
    ax1_twin.legend()

    frames = dyn.shape[0]

    widget_spec = GridSpecFromSubplotSpec(1, 2, grid[2], width_ratios=[.8, .2])
    ax_slider = fig.add_subplot(widget_spec[0, 0])
    frame_sel = Slider(
        ax=ax_slider,
        label="t",
        valmin=0,
        valmax=tfin,
        valinit=0,
        valstep=dt,
    )
    ax_button = fig.add_subplot(widget_spec[0, 1])
    anim_button = Button(
        ax=ax_button,
        label="Animate",
    )

    frames_gen = range(0, frames, nth)

    def animate(frame):
        t, r, psi0_, psi1_ = dyn[frame].T
        lines0.set_data(r, psi0_)
        lines1.set_data(r, psi1_)
        R_ = R[frame]
        _, R_gs, R_es = R_
        _, norm_gs, norm_es = norms[frame]
        _, norm_ionic = norms_ionic[frame]
        vline_gs.set_xdata([R_gs, R_gs])
        vline_es.set_xdata([R_es, R_es])
        scatter_laser.set_offsets(laser[frame])
        ax0_twin.set_title(
                f"t={t[0]:.2f} fs, norm_gs={norm_gs:.6f}, norm_es={norm_es:.6f}, "
                f"norm_ionic={norm_ionic: >14.4f}"
        )

    def start_anim(event=None):
        global anim
        anim = animation.FuncAnimation(fig, animate, frames_gen, interval=args.interval)
        return anim

    def set_frame_for_t(frame):
        global anim
        try:
            anim.pause()
        except AttributeError:
            pass
        anim = None
        # frame = int(t / dt) - 1
        frame = int(frame / dt) - 1
        animate(frame)

    frame_sel.on_changed(set_frame_for_t)
    anim_button.on_clicked(start_anim)

    anim = start_anim()

    _, ax_spec = plt.subplots()
    spectrum = np.loadtxt(out / "spectrum.dat")
    ax_spec.plot(*spectrum.T)
    ax_spec.set_xlim(0., 5.)
    ax_spec.set_xlabel("E / eV")
    ax_spec.set_ylabel("Intensity / arb. units.")
    ax_spec.set_title("Photoelectron-spectrum")

    fig.tight_layout()
    plt.show()


if __name__ == "__main__":
    run()
